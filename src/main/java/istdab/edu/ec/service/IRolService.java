package istdab.edu.ec.service;

import java.util.List;

import javax.ejb.Local;

import istdab.edu.ec.model.Rol;
import istdab.edu.ec.model.Usuario;

@Local
public interface IRolService extends IService<Rol>{
	Integer asignar(Usuario us, List<Rol> roles);
}
