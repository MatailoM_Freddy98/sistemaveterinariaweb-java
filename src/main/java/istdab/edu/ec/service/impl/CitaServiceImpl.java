package istdab.edu.ec.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.inject.Named;

import istdab.edu.ec.dao.ICitaDAO;
import istdab.edu.ec.model.Cita;
import istdab.edu.ec.service.ICitaService;


@Named
public class CitaServiceImpl implements ICitaService, Serializable{

	@EJB
	private ICitaDAO dao;
	
	@Override
	public Integer registrar(Cita t) throws Exception {
		// TODO Auto-generated method stub
		return dao.registrar(t);
	}

	@Override
	public Integer modificar(Cita t) throws Exception {
		// TODO Auto-generated method stub
		return dao.modificar(t);
	}

	@Override
	public Integer eliminar(Cita t) throws Exception {
		// TODO Auto-generated method stub
		return dao.eliminar(t);
	}

	@Override
	public List<Cita> listar() throws Exception {
		// TODO Auto-generated method stub
		return dao.listar();
	}

	@Override
	public Cita listarPorId(Cita t) throws Exception {
		// TODO Auto-generated method stub
		return dao.listarPorId(t);
	}

}
