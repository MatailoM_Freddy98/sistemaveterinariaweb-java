package istdab.edu.ec.service.impl;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.mindrot.jbcrypt.BCrypt;

import istdab.edu.ec.dao.IPersonaDAO;
import istdab.edu.ec.dao.IUsuarioDAO;
import istdab.edu.ec.model.Usuario;
import istdab.edu.ec.service.IUsuarioService;

@Named
public class UsuarioServiceImpl implements IUsuarioService,Serializable {
	
	@PersistenceContext(unitName = "supPU")
	private EntityManager em;
	
	@EJB
	IUsuarioDAO dao;
	
	@Override
	public Integer registrar(Usuario t) throws Exception {
		// TODO Auto-generated method stub
		return dao.registrar(t);
	}

	@Override
	public Integer modificar(Usuario t) throws Exception {
		// TODO Auto-generated method stub
		return dao.modificar(t);
	}

	@Override
	public Integer eliminar(Usuario t) throws Exception {
		// TODO Auto-generated method stub
		return dao.eliminar(t);
	}

	@Override
	public List<Usuario> listar() throws Exception {
		// TODO Auto-generated method stub
		return dao.listar();
	}

	@Override
	public Usuario listarPorId(Usuario t) throws Exception {
		// TODO Auto-generated method stub
		return dao.listarPorId(t);
	}

	@Override
	public Usuario login(Usuario us) {
		
		Usuario usuario = null;
		
		String clave = us.getClave();
		String claveHash = dao.traerPassHashed(us.getUsuario());
		try {
			if (!claveHash.isEmpty()) {
				if (BCrypt.checkpw(clave, claveHash)) { //Bcyrpt checkpw compara la clave encriptada con la clave desencriptada
					return dao.leerPorNombreUsuario(us.getUsuario()); // us.getusuario envia el nombre de usuario
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			throw e;
		}
		return null;
	}

}
