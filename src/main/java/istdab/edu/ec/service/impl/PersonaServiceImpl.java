package istdab.edu.ec.service.impl;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.inject.Named;

import istdab.edu.ec.dao.IPersonaDAO;
import istdab.edu.ec.model.Persona;
import istdab.edu.ec.service.IPersonaService;

@Named
public class PersonaServiceImpl implements IPersonaService,Serializable{

	@EJB
	private IPersonaDAO dao;
	
	@Override
	public Integer registrar(Persona t) throws Exception {
		// TODO Auto-generated method stub
		return dao.registrar(t);
	}

	@Override
	public Integer modificar(Persona t) throws Exception {
		// TODO Auto-generated method stub
		return dao.modificar(t);
	}

	@Override
	public Integer eliminar(Persona t) throws Exception {
		// TODO Auto-generated method stub
		return dao.eliminar(t);
	}

	@Override
	public List<Persona> listar() throws Exception {
		// TODO Auto-generated method stub
		return dao.listar();
	}

	@Override
	public Persona listarPorId(Persona t) throws Exception {
		// TODO Auto-generated method stub
		return dao.listarPorId(t);
	}

}
