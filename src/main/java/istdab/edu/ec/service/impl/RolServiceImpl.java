package istdab.edu.ec.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.inject.Named;

import istdab.edu.ec.dao.IRolDAO;
import istdab.edu.ec.model.Rol;
import istdab.edu.ec.model.Usuario;
import istdab.edu.ec.model.UsuarioRol;
import istdab.edu.ec.service.IRolService;

@Named
public class RolServiceImpl implements IRolService,Serializable{

	@EJB
	private IRolDAO dao;
	
	@Override
	public Integer registrar(Rol t) throws Exception {
		// TODO Auto-generated method stub
		return dao.registrar(t);
	}

	@Override
	public Integer modificar(Rol t) throws Exception {
		// TODO Auto-generated method stub
		return dao.modificar(t);
	}

	@Override
	public Integer eliminar(Rol t) throws Exception {
		// TODO Auto-generated method stub
		return dao.eliminar(t);
	}

	@Override
	public List<Rol> listar() throws Exception {
		// TODO Auto-generated method stub
		return dao.listar();
	}

	@Override
	public Rol listarPorId(Rol t) throws Exception {
		// TODO Auto-generated method stub
		return dao.listarPorId(t);
	}

	@Override
	public Integer asignar(Usuario us, List<Rol> roles) {
		// TODO Auto-generated method stub
		
		List<UsuarioRol> usuario_roles = new ArrayList<>();
		roles.forEach(r ->{
			UsuarioRol ur = new UsuarioRol(); // va a crear un usuario rol en bd
			ur.setUsuario(us);
			ur.setRol(r);
			usuario_roles.add(ur);
		});
		return dao.asignar(us, usuario_roles);
	}

}
