package istdab.edu.ec.service;

import istdab.edu.ec.model.Usuario;

public interface IUsuarioService extends IService<Usuario>{
	Usuario login(Usuario us);
}
