package istdab.edu.ec.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

import org.mindrot.jbcrypt.BCrypt;

import istdab.edu.ec.model.Persona;
import istdab.edu.ec.model.Rol;
import istdab.edu.ec.model.Usuario;
import istdab.edu.ec.service.IPersonaService;
import istdab.edu.ec.service.IRolService;

@Named
@ViewScoped
public class RegistrarBean implements Serializable{
	
	@Inject
	private IPersonaService personaService;
	
	@Inject
	private IRolService rolService;
	
	private Persona persona;
	private Usuario usuario;
	
	@PostConstruct
	private void init() {
		this.persona = new Persona();
		this.usuario = new Usuario();
	}
	
	//metodos
	@Transactional
	public String registrar() {
		String redireccion = "";
		try {
			String clave = this.usuario.getClave();
			String claveHash  = BCrypt.hashpw(clave, BCrypt.gensalt());
			this.usuario.setClave(claveHash);
			this.usuario.setPersona(persona);
			this.persona.setUsu(usuario);
			this.personaService.registrar(persona);
			
			//Lista de roles para agregar al usuario
			List<Rol> roles = new ArrayList<>();
			Rol r = new Rol();
			r.setId(1);
			roles.add(r);
			
			rolService.asignar(this.usuario, roles);
			redireccion="index?faces-redirect=true";
		} catch (Exception e) {
			// TODO: handle exception
		}
		return redireccion;
	}

	/**
	 * 
	 * @metodos Get y Set
	 */
	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	
}
