package istdab.edu.ec.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.Init;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import istdab.edu.ec.model.Usuario;
import istdab.edu.ec.service.IUsuarioService;


@Named
@ViewScoped
public class IndexBean implements Serializable{
	
	private Usuario us;
	
	@Inject
	private IUsuarioService service;
	
	private boolean existeRegistro = false;
	private List<Usuario> listaUsuarios;

	@PostConstruct
	public void init() {
		this.us = new Usuario();
		this.listarUsuarios();
	}
	
	public String login() {
		String redireccion="";
		try {
			Usuario usuario = service.login(us);
			if(usuario != null && usuario.getEstado().equalsIgnoreCase("A")) {
				//FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Aviso", "CORRECTO!!!"));
				FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("usuario", usuario);
				redireccion = "/protegido/citas-medicas?faces-redirect=true"; // Vista Principal especificando la carpeta 
			}else {
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Aviso", "Credenciales Incorrectas"));
			}
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Aviso", e.getMessage()));
		}
		return redireccion;
	}
	
	public void listarUsuarios() {
		try {
			this.listaUsuarios = this.service.listar(); // la lista es traida desde la base de datos de DAO, luego del dao trae al servicio y finalemnte al Bean. 
			if(listaUsuarios.isEmpty()) {
				this.existeRegistro = false;
			}else {
				this.existeRegistro = true;
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}
	
	
	public Usuario getUs() {
		return us;
	}

	public void setUs(Usuario us) {
		this.us = us;
	}

	public boolean isExisteRegistro() {
		return existeRegistro;
	}

	public void setExisteRegistro(boolean existeRegistro) {
		this.existeRegistro = existeRegistro;
	}
}
