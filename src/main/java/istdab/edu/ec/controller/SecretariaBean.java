package istdab.edu.ec.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;


import istdab.edu.ec.model.Persona;
import istdab.edu.ec.model.Rol;
import istdab.edu.ec.model.Usuario;
import istdab.edu.ec.service.IPersonaService;
import istdab.edu.ec.service.IRolService;
import istdab.edu.ec.service.IUsuarioService;

@Named
@ViewScoped
public class SecretariaBean implements Serializable{
	
	@Inject
	private IPersonaService personaService;
	
	@Inject
	private IRolService rolService;
	
	@Inject
	private IUsuarioService usuarioService;

	private Persona persona;
	private Usuario usuario;
	private String tipoDialogo;
	private List<Usuario> lista;
	
	
	@PostConstruct
	private void init() {
		this.persona = new Persona();
		this.usuario = new Usuario();
		this.lista = new ArrayList<>();
		this.listarSecretarias();
	}
	
	@Transactional
	public void operar(String accion) {
		try {
			if(accion.equalsIgnoreCase("R")) {
				this.personaService.registrar(persona);
			}else if(accion.equalsIgnoreCase("M")) {
				this.personaService.modificar(persona);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	//metodos
	@Transactional
	public void registrar() {
		try {
			this.usuario.setPersona(persona);
			this.persona.setUsu(usuario);
			this.personaService.registrar(persona);
			
			//Lista de roles para agregar al usuario
			List<Rol> roles = new ArrayList<>();
			Rol r = new Rol();
			r.setId(3);
			roles.add(r);
			
			rolService.asignar(this.usuario, roles);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	public void listarSecretarias() {
		try {
			 this.lista = this.usuarioService.listar();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}
	public void limpiarControles() {
		this.persona = new Persona();
		this.usuario = new Usuario();
		this.tipoDialogo = "Nueva Secretaria";
	}

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public List<Usuario> getLista() {
		return lista;
	}

	public void setLista(List<Usuario> lista) {
		this.lista = lista;
	}

	public String getTipoDialogo() {
		return tipoDialogo;
	}

	public void setTipoDialogo(String tipoDialogo) {
		this.tipoDialogo = tipoDialogo;
	}
	
}
