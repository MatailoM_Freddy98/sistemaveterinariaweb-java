package istdab.edu.ec.controller;

import java.io.Serializable;

import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import istdab.edu.ec.model.Usuario;



@Named
@ViewScoped
public class MasterBean implements Serializable{
	
	public void verificarSesion() throws Exception{
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			Usuario us = (Usuario) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("usuario");
			if(us == null) {
				// Si es null redirige al login
				context.getExternalContext().redirect("./../index.xhtml");
			}
		} catch (Exception e) {
			context.getExternalContext().redirect("./../index.xhtml");
		}
	}
	
	public void cerrarSesion() {
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();// cerrar sesion metodo.
	}
}
