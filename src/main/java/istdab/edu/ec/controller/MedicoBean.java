package istdab.edu.ec.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

import org.mindrot.jbcrypt.BCrypt;

import istdab.edu.ec.model.Persona;
import istdab.edu.ec.model.Rol;
import istdab.edu.ec.model.Usuario;
import istdab.edu.ec.service.IPersonaService;
import istdab.edu.ec.service.IRolService;
import istdab.edu.ec.service.IUsuarioService;

@Named
@ViewScoped
public class MedicoBean implements Serializable{
	
	@Inject
	private IPersonaService personaService;
	
	@Inject
	private IUsuarioService usuarioService;
	
	@Inject
	private IRolService rolService;
	
	private List<Usuario> listaMedicos;
	
	private Persona persona;
	private Usuario usuario;
	private String tipoDialogo;
	
	@PostConstruct
	private void init() {
		this.persona = new Persona();
		this.usuario = new Usuario();
		this.listaMedicos = new ArrayList<>();
		this.listarMed();
	}
	
	//metodos
	@Transactional
	public void registrar() {
		try {
			String clave = this.usuario.getClave();
			String claveHash  = BCrypt.hashpw(clave, BCrypt.gensalt());
			this.usuario.setClave(claveHash);
			this.usuario.setPersona(persona);
			this.persona.setUsu(usuario);
			this.personaService.registrar(persona);
			
			//Lista de roles para agregar al usuario
			List<Rol> roles = new ArrayList<>();
			Rol r = new Rol();
			r.setId(1);
			roles.add(r);
			
			rolService.asignar(this.usuario, roles);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	public void listarMed() {
		try {
			 this.listaMedicos = this.usuarioService.listar();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}
	public void limpiarControles() {
		this.persona = new Persona();
		this.usuario = new Usuario();
		this.tipoDialogo = "Nuevo Medico";
	}

	/**
	 * 
	 * @metodos Get y Set
	 */
	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public List<Usuario> getListaMedicos() {
		return listaMedicos;
	}

	public void setListaMedicos(List<Usuario> listaMedicos) {
		this.listaMedicos = listaMedicos;
	}

	public String getTipoDialogo() {
		return tipoDialogo;
	}

	public void setTipoDialogo(String tipoDialogo) {
		this.tipoDialogo = tipoDialogo;
	}
	
	
}
