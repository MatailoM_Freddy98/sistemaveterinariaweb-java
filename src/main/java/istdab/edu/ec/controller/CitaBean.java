package istdab.edu.ec.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;


import istdab.edu.ec.model.Cita;
import istdab.edu.ec.model.Usuario;
import istdab.edu.ec.service.ICitaService;
import istdab.edu.ec.service.IUsuarioService;

@Named
@ViewScoped
public class CitaBean implements Serializable{
	
	@Inject
	private ICitaService serviceCita;
	
	
	@Inject
	private IUsuarioService servicePersonas;
	
	private Cita cita;
	
	private List<Cita> listaCitas;
	private List<Usuario> listaMedicos;
	private List<Usuario> listaPacientes;
	private String tipoDialogo;
	
	@PostConstruct
	private void init() {
		this.cita = new Cita();
		this.listarCitas();
		this.listarMedicos();
		this.listarPacientes();
	}
	
	//metodos
	@Transactional
	public void operar(String accion) {
		try {
			if(accion.equalsIgnoreCase("R")) {
				this.serviceCita.registrar(cita);
			}else if(accion.equalsIgnoreCase("M")) {
				this.serviceCita.modificar(cita);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	
	public void mostrarData(Cita c) {
		this.cita = c;
		this.tipoDialogo = "Modificar Cita";
	}

	public void listarCitas() {
		try {
			this.listaCitas = this.serviceCita.listar();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	
	public void listarMedicos() {
		try {
			this.listaMedicos = this.servicePersonas.listar();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	
	public void listarPacientes() {
		try {
			this.listaPacientes = this.servicePersonas.listar();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	
	public void limpiarControles() {
		this.cita = new Cita();
		this.tipoDialogo = "Nueva Cita";
	}

	public Cita getCita() {
		return cita;
	}

	public void setCita(Cita cita) {
		this.cita = cita;
	}

	public List<Cita> getListaCitas() {
		return listaCitas;
	}

	public void setListaCitas(List<Cita> listaCitas) {
		this.listaCitas = listaCitas;
	}

	public List<Usuario> getListaMedicos() {
		return listaMedicos;
	}

	public void setListaMedicos(List<Usuario> listaMedicos) {
		this.listaMedicos = listaMedicos;
	}

	public List<Usuario> getListaPacientes() {
		return listaPacientes;
	}

	public void setListaPacientes(List<Usuario> listaPacientes) {
		this.listaPacientes = listaPacientes;
	}

	public String getTipoDialogo() {
		return tipoDialogo;
	}

	public void setTipoDialogo(String tipoDialogo) {
		this.tipoDialogo = tipoDialogo;
	}
}
