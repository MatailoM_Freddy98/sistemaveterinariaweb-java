package istdab.edu.ec.dao;

import istdab.edu.ec.model.Usuario;

public interface IUsuarioDAO extends ICrud<Usuario>{
	String traerPassHashed(String us);
	Usuario leerPorNombreUsuario(String nombre); // Metodo Traer todo los datos del Usuario
}
