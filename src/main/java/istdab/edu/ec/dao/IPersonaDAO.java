package istdab.edu.ec.dao;

import java.util.List;

import javax.ejb.Local;

import istdab.edu.ec.model.Persona;


//en contexto de la misma maquina virtual
@Local
public interface IPersonaDAO extends ICrud<Persona>{
	//La interface ICRUD realiza todos los procesos para registro de Personas
}
