package istdab.edu.ec.dao.impl;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import istdab.edu.ec.dao.ICitaDAO;
import istdab.edu.ec.model.Cita;



@Stateful
public class CitaDAOImpl implements ICitaDAO, Serializable{

	@PersistenceContext(unitName = "supPU")
	private EntityManager em;
	
	@Override
	public Integer registrar(Cita t) throws Exception {
		em.persist(t);
		return t.getId();
	}

	@Override
	public Integer modificar(Cita t) throws Exception {
		em.merge(t);
		return t.getId();
	}

	@Override
	public Integer eliminar(Cita t) throws Exception {
		em.remove(em.merge(t));
		return 1;
	}

	@Override
	public List<Cita> listar() throws Exception {
		// JPQL Java Persistence Query Language
		Query q = em.createQuery("SELECT e FROM Cita e");
		List<Cita> lista =(List<Cita>) q.getResultList();
		return lista;
	}

	@Override
	public Cita listarPorId(Cita t) throws Exception {
		Query q = em.createQuery("FROM Cita c WHERE c.id = ?");
		q.setParameter(1, t.getId());
		List<Cita> lista =(List<Cita>) q.getResultList();
		return lista != null && !lista.isEmpty() ? lista.get(0) : new Cita();
	}


}
