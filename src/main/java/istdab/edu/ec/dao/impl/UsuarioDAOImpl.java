package istdab.edu.ec.dao.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateful;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import istdab.edu.ec.dao.IUsuarioDAO;
import istdab.edu.ec.model.Usuario;


@Stateless
public class UsuarioDAOImpl implements IUsuarioDAO,Serializable{

	@PersistenceContext(unitName = "supPU")
	private EntityManager em;
	
	@Override
	public Integer registrar(Usuario t) throws Exception {
		// TODO Auto-generated method stub
		em.persist(t);
		return t.getPersona().getIdPersona();
	}

	@Override
	public Integer modificar(Usuario t) throws Exception {
		// TODO Auto-generated method stub
		em.merge(t);
		return t.getPersona().getIdPersona();
	}

	@Override
	public Integer eliminar(Usuario t) throws Exception {
		// TODO Auto-generated method stub
		em.remove(em.merge(t));
		return 1;
	}

	@Override
	public List<Usuario> listar() throws Exception {
		// TODO Auto-generated method stub
		List<Usuario> lista = new ArrayList<Usuario>();
		try {
			Query query = em.createQuery("SELECT u FROM Usuario u"); //JPQL es en base a entidad
			lista =(List<Usuario>) query.getResultList();
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		}
		return lista;
	}

	@Override
	public Usuario listarPorId(Usuario t) throws Exception {
		// TODO Auto-generated method stub
		Usuario us = new Usuario();
		List<Usuario> lista = new ArrayList<Usuario>();
		try {
			Query query = em.createQuery("FROM Usuario u WHERE u.id = ?1");
			query.setParameter(1, t.getPersona().getIdPersona());
			
			lista =(List<Usuario>) query.getResultList();
			if (lista != null && !lista.isEmpty()) {
				us = lista.get(0);
			}
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		}
		return us;
	}

	@Override
	public String traerPassHashed(String us) {
		
		Usuario usuario = new Usuario();
		try {
			//SQL select clave from usuario where usuario = 'NOMBRE';
			Query query = em.createQuery("FROM Usuario u WHERE u.usuario = ?1");
			query.setParameter(1, us);
			
			List<Usuario> lista =(List<Usuario>) query.getResultList();
			if (!lista.isEmpty()) {
				usuario = lista.get(0);
			}
		} catch (Exception e) {
			throw e;
		}
		return usuario != null && usuario.getId() != null ? usuario.getClave() : " ";
	}

	@Override
	public Usuario leerPorNombreUsuario(String nombre) {
		
		Usuario usuario = new Usuario();
		try {
			Query query = em.createQuery("FROM Usuario u WHERE u.usuario = ?1"); // U usuario = nombre que se le pasa a la variable nombre
			query.setParameter(1, nombre);
			
			List<Usuario> lista = (List<Usuario>) query.getResultList();
			if (!lista.isEmpty()) {
				usuario = lista.get(0);
			}
		} catch (Exception e) {
			throw e; // subir un error a una capa superior
		}
		return usuario;
	}

}
