package istdab.edu.ec.dao.impl;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import istdab.edu.ec.dao.IRolDAO;
import istdab.edu.ec.model.Rol;
import istdab.edu.ec.model.Usuario;
import istdab.edu.ec.model.UsuarioRol;


@Stateful
public class RolDAOImpl implements IRolDAO,Serializable {

	@PersistenceContext(unitName = "supPU")
	private EntityManager em;
	
	
	@Override
	public Integer registrar(Rol t) throws Exception {
		em.persist(t);
		return t.getId();
	}

	@Override
	public Integer modificar(Rol t) throws Exception {
		em.merge(t);
		return t.getId();
	}

	@Override
	public Integer eliminar(Rol t) throws Exception {
		em.remove(em.merge(t));
		return 1;
	}

	@Override
	public List<Rol> listar() throws Exception {
		//JPQL JAVA PERSISTENCE QUERY Language Orientado a Entidades.
		Query q = em.createQuery("SELECT r FROM Rol r");
		List<Rol> lista = (List<Rol>) q.getResultList(); //
		return lista;
	}

	@Override
	public Rol listarPorId(Rol t) throws Exception {
		// Ejemplo normal SQL, Native its nativo.
		//Query q = em.createNativeQuery("FROM Rol r WHERE r.id = ?");
		Query q = em.createQuery("FROM Rol r WHERE r.id = ?");
		q.setParameter(1, t.getId());
		List<Rol> lista = (List<Rol>) q.getResultList(); //
		return lista != null && !lista.isEmpty() ? lista.get(0): new Rol(); // if inline , ese signo ? significa si se cumplio la condicion
	}

	@Override
	public Integer asignar(Usuario us, List<UsuarioRol> roles) {
		//
		Query q = em.createNativeQuery("DELETE FROM usuario_rol ur WHERE ur.id_usuario = ?");
		q.setParameter(1, us.getPersona().getIdPersona());
		q.executeUpdate();
		
		int[] i = {0}; 
		roles.forEach(r ->{ //Es como el for
			em.persist(r);
			if (i[0]%100==0) {
				em.flush();
				em.clear();
			}
			i[0]++;
		});
		return i[0]; //
	}

}
