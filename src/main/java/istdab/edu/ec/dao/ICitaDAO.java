package istdab.edu.ec.dao;

import java.util.List;

import javax.ejb.Local;

import istdab.edu.ec.model.Cita;


@Local
public interface ICitaDAO extends ICrud<Cita>{
}
