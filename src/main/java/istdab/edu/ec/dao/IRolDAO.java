package istdab.edu.ec.dao;

import java.util.List;

import javax.ejb.Local;

import istdab.edu.ec.model.Rol;
import istdab.edu.ec.model.Usuario;
import istdab.edu.ec.model.UsuarioRol;


@Local
public interface IRolDAO extends ICrud<Rol>{
	
	Integer asignar (Usuario us, List<UsuarioRol> roles);
	
	
}
